<?php
    session_start();
    include "db.php";

    //echo $_SESSION['user_id']."<br>";
    //echo $_SESSION['Email']."<br>";
    //echo $_SESSION['Img']."<br>";

    if (!isset($_SESSION['user_id'])){
        header("Location:LogIn.html");
    }
?>

<!DOCTYPE html>
<html lang="en" dir="ltr"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <link rel="stylesheet" href="style.css">
    <title>No-Legalo | Profile Page</title>
  </head>
  <body>
  
      <div class="NameAndLozung">
        <span class="name">No- <span style="color: #333333;">Legalo</span> </span>
        <br>
        <span class="lozung">Buy Here - Die There</span>
      </div>

     <div id="header">
       <ul>
          <li><a id="MainPageLink" class="active" href="index.php">Home</a></li>
          <li><a id="ShopLink" href="ShopPage.php">Sales</a></li>
          <li><a id="ProfileLink" href="ProfilePage.php">Profile</a></li>
          <li class="Cart" style="float:right"><a id="CartLink" href="Cart.php" >Cart</a></li>          
          <li class="Login" style="float:right" onclick="document.getElementById(&#39;id01&#39;).style.display=&#39;block&#39;"><a id="LoginLink" href="LogIn.html">Log In / Out</a></li>
          <li class="GEO" onclick="changelagunge()" style="float:right"> <a id="LanguageButtonText">GEO</a> </li>
          <li class="Moreoptions" onclick="ShowMenuOptions()" style="float:right"> <a>|||</a> </li>
       </ul>
       </div>
       <div id="MoreoptionsBox" onmouseleave="HideMenuOptions()">
          <div class="option" onclick="changelagunge()" id="LanguageButtonText">GEO</div>
          <div><a href="LogIn.html" class="option">Log In</a></div>
          <div><a href="" class="option">Cart</a></div>
      </div>
     
     <br>
     <div class="recbox">
        <div class="recleft"> <img class="ad" src="img/rec1.gif" alt=""> </div>
        <div class="recright"> <img class="ad" src="img/rec1.gif" alt=""> </div>
     </div>

     <div class="rectop"> <img class="ad2" src="img/rec2.jpg" alt=""> </div>

     <div class="main"> 
        <div class="UserImgBox">
            <img style="width:inherit; height:200px" src="img/<?php echo $_SESSION['Img'] ?>">
        </div>
        <div class="UserInfo">
            <h2>
                <?php echo $_SESSION['Email']; ?> 
            </h2>
            <a class="UserLink" href="?action=posts">My Posts</a> 
            <a class="UserLink" href="?action=comments">My Comments</a> 
            <a class="UserLink" href="?action=orders">My Orders</a>
            <a class="UserLink" href="?action=edit">Edit Profile</a>  
        </div>
        <div class="UserDisplay">
            <?php 
                if(isset($_GET['action'])){
                    switch ($_GET['action']){
                        case 'edit';
                            include "ProfileDisplays/edit.php";
                            break;
                        case 'posts';
                            include "ProfileDisplays/posts.php";
                            break;    
                        case 'comments';
                            include "ProfileDisplays/comments.php";
                            break;
                        case 'orders';
                            include "ProfileDisplays/orders.php";
                            break;
                        case 'NewPost';
                            include "ProfileDisplays/NewUserPost.php";
                            break;  
                        case 'UpdatePost';
                            include "ProfileDisplays/UpdatePost.php";
                            break;          
                    }
                }
            ?>
        </div>
        
     </div>
       

     <div class="adtop" style="margin-top:20px;"> <img class="ad2" src="img/rec2.jpg" alt=""> </div>
     <script src="main.js"></script>


  

</body></html>