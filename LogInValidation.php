<?php 
    session_start();
    include "db.php";

    if ($_POST['Email'] != "" && $_POST['Pass'] != "" ){
        $NotFound = 0;
        $select = "SELECT * FROM users";
        $result = mysqli_query($conn, $select);
        while ($arr = mysqli_fetch_assoc($result)){
            $hash = $arr['Passsword'];
            if ($arr['Email'] == $_POST['Email'] && password_verify($_POST['Pass'], $hash)){
                $NotFound = 0;
                $_SESSION['user_id'] = $arr['id_u'];
                $_SESSION['Email'] = $arr['Email'];
                $_SESSION['Img'] = $arr['Img'];

                if(isset($_POST['RemMe'])){
                    if($_POST['RemMe'] == 'Yes'){
                        setcookie('user_id', $arr['id_u'], time()+3600);
                        setcookie('Email', $arr['Email'], time()+3600);
                        setcookie('Img', $arr['Img'], time()+3600);
                    }
                }
                else{
                    setcookie('user_id', "", time() - 3600);
                    setcookie('Email', "", time() - 3600);
                    setcookie('Img', "", time() - 3600);
                }
                header("Location:ProfilePage.php");
                break;
                
            }
            else{
                $NotFound = 1;
            }
        }
        if ($NotFound == 1){
            include "LogIn.html";
            echo "<div style='width:80%; height:contain;background:#333;color:red;margin:auto;padding:10px;'>". "Incorrect Email or password". "</div>";
        }
    }
    else{
        header("Location:LogIn.html");
    }
?>