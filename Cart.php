<?php
    session_start();
    include "db.php";

    if(!isset($_SESSION['user_id'])){
        header("Location:LogIn.html");
    }
    $user_id=$_SESSION['user_id'];
    $selectCart = "SELECT * FROM cart WHERE user_fk = '$user_id'";
    $resultCart = mysqli_query($conn, $selectCart);

    $selectUser = "SELECT * FROM users WHERE id_u = '$user_id'";
    $resultUser = mysqli_query($conn, $selectUser);
    $arrUser =  mysqli_fetch_assoc($resultUser);

    if(isset($_GET['RemoveCartID'])){
      $RemoveCartID=$_GET['RemoveCartID'];
      $delete = "DELETE FROM cart WHERE id_cart = '$RemoveCartID'";
      mysqli_query($conn, $delete);
      header("Location:Cart.php");
    }
    $TotalPrice = 0;
    $ProductCount = 0;
    
?>

<!DOCTYPE html>
<html lang="en" dir="ltr"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="ProfileDisplays/displayStyle.css">
    <title>No-Legalo | Cart</title>
  </head>
  <body>
  
      <div class="NameAndLozung">
        <span class="name">No- <span style="color: #333333;">Legalo</span> </span>
        <br>
        <span class="lozung">Buy Here - Die There</span>
      </div>

     <div id="header">
       <ul>
          <li><a id="MainPageLink" class="active" href="index.php">Home</a></li>
          <li><a id="ShopLink" href="ShopPage.php">Sales</a></li>
          <li><a id="ProfileLink" href="ProfilePage.php">Profile</a></li>
          <li class="Cart" style="float:right"><a id="CartLink" href="Cart.php" >Cart</a></li>
          <li class="Login" style="float:right" onclick="document.getElementById(&#39;id01&#39;).style.display=&#39;block&#39;"><a id="LoginLink" href="LogIn.html">Log In / Out</a></li>
          <li class="GEO" onclick="changelagunge()" style="float:right"> <a id="LanguageButtonText">GEO</a> </li>
          <li class="Moreoptions" onclick="ShowMenuOptions()" style="float:right"> <a>|||</a> </li>
       </ul>
       </div>
       <div id="MoreoptionsBox" onmouseleave="HideMenuOptions()">
          <div class="option" onclick="changelagunge()" id="LanguageButtonText">GEO</div>
          <div><a href="LogIn.html" class="option">Log In</a></div>
          <div><a href="" class="option">Cart</a></div>
      </div>
     
     <br>
     <div class="recbox">
        <div class="recleft"> <img class="ad" src="img/rec1.gif" alt=""> </div>
        <div class="recright"> <img class="ad" src="img/rec1.gif" alt=""> </div>
     </div>

     <div class="rectop"> <img class="ad2" src="img/rec2.jpg" alt=""> </div>

     <div class="main" style="padding-bottom:30px;"> 
        <h2 style="color:white;">My Cart</h2>
        <hr style="border:1px solid #464646;margin-bottom:30px;">
        <form action="PurchaseOperation.php" method="post">
        <?php while ($arr = mysqli_fetch_assoc($resultCart)){
          $product_id = $arr['product_fk'];
          $selectProduct = "SELECT * FROM products WHERE id_pr ='$product_id'";
          $resultProduct = mysqli_query($conn, $selectProduct);
          $arrProduct =  mysqli_fetch_assoc($resultProduct);
          $OverallProductPrice = $arrProduct['price']*$arr['quantity'];
          $ProductCount++;
        ?>
        <input type="hidden" name="ProductId<?php echo $ProductCount;  ?>" value="<?php echo $arrProduct['id_pr'] ?>">

        <div class='postbox' style="margin-top:10px;margin-left:auto;margin-right:auto;">
              <div class='postimgbox'><img src='img/<?php echo $arrProduct['mainImg'] ?>' style='width:inherit; height:inherit'></div>
              <div class='posttextbox'>
                <div class='postheading'><?php echo $arrProduct['name'] ?></div>
                <hr style='margin-top:5px;margin-bottom:5px;'>
                <div class='postshorttext'> <span style="color:rgb(0, 129, 28)"><?php echo $arrProduct['availability'] ?></span> | Quantity: <?php echo $arr['quantity'];  ?> | Price: <span style="color:rgb(0, 129, 28)"><?php echo $OverallProductPrice ?>$</span> <tr>  </div>
              <span><a class='postmore cartbtn cartbtn1' href='cart.php?RemoveCartID=<?php echo $arr['id_cart'] ?>'>Remove</a></span>
              <span><a class='postmore cartbtn ' href='ProductPage.php?ProductId=<?php echo $arrProduct['id_pr'] ?>'>View Products page  </a></span>
              </div>
        </div>
        <?php 
         $TotalPrice += $OverallProductPrice;
         //echo $TotalPrice;
        }
        ?>
        <input type="hidden" name="UserId" value="<?php echo $user_id; ?>">
        <input type="hidden" name="Totalprice" value="<?php echo $TotalPrice; ?>">
        <input type="hidden" name="ProductCount" value="<?php echo $ProductCount; ?>">
        <hr style="border:1px solid #464646;margin-top:30px;width:100%"><br>
        <div class="CheckOutDiv">
        Total Price: <span style="color:rgb(0, 129, 28);margin-right:15%;"><?php echo $TotalPrice; ?> $ </span> 
          <br>
          <input type="submit" style="float:none;width:150px;border:0.5px solid green;color:green;background:transparent;border-radius:5px;margin-right:15%;margin-top:10px;" value="Proceed to checkout" >
          </form>
        </div>
    
        <?php 
          if (isset($_GET['event'])){
            if ($_GET['event'] == 'bought'){
              echo "<span style='font-size:20px; color:green;'> You have succesfult orderd products from your cart <br>
               You can view your orders on profile page </span>";
            }
            if ($_GET['event'] == 'cantbuy'){
              echo "<span style='font-size:20px; color:green;'> The cart is empty </span>";

            }
          } 
        ?>
        
     
     
    </div>

  

     <div class="adtop" style="margin-top:20px;"> <img class="ad2" src="img/rec2.jpg" alt=""> </div>
     <script src="main.js"></script>


  

</body></html>