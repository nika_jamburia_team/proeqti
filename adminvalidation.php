<?php 
    session_start();
    include "db.php";
    if(isset($_POST['adminIn'])){
        $AdminPass = $_POST['Adminpass'];
        $correctPass = false;
        $select = "SELECT * FROM users";
        $result = mysqli_query($conn, $select);
        while($arr = mysqli_fetch_assoc($result)){
            $hash = $arr['Passsword'];
            if (password_verify($AdminPass, $hash)){
                $correctPass = true;
                $_SESSION['admin'] = $arr['Email'];
                $_SESSION['admin_id'] = $arr['id_u'];

                $_SESSION['user_id'] = NULL;
                $_SESSION['Email'] = NULL;
                $_SESSION['Img'] = NULL;
                
                if(isset($_COOKIE['user_id'])){
                    setcookie('user_id', "", time() - 3600);
                    setcookie('Email', "", time() - 3600);
                    setcookie('Img', "", time() - 3600);
                }

                header("Location:adminpanel.php");
                break;
            }
        }
        if(!$correctPass){
            header("Location:admin.php?msg=WrongPass");
        }
    }
    else{
        header("Location:index.php");
    }
?>