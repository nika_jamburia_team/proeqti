<?php 
    if (!isset($_SESSION['admin'])){
        header("Location:../index.php");
    }
    else{
        include "db.php";
        $today = date("Y-m-d");
        $admin_id = $_SESSION['admin_id'];
        $admin = $_SESSION['admin'];
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="ProfileDisplays/displayStyle.css">
</head>
<body>
    <form action="" method="post" enctype="multipart/form-data">
        <p>Category:</p>
        <select name="NewPostCat" id="">
            <option value="1">News</option>
            <option value="2">User Posts</option>
            <option value="3">Other</option>
        </select>
        <p>Heading:</p>
        <input type="text" name="NewPostHeading" style="width: 40%;" maxlength="50" required >
        <div style="width:auto;height:auto;">
            <p>Image:</p>
            <input type="file" name="NewPostImg">
        </div>
        <p>Main text:</p>
        <small style="color:grey">You can't type more than 5000 characters</small> <br>
        <textarea class="NewCommtxt" name="NewPostText" cols="80" rows="30" maxlength="5000" required ></textarea>
        <br>
        <input type="submit" name="NewPostIn" value="Add Post">
    </form>

    <?php
        if(isset($_POST['NewPostIn'])){
            $NewPostCat = $_POST['NewPostCat'];
            $NewPostHeading = $_POST['NewPostHeading'];
            $NewPostText = mysqli_real_escape_string($conn, $_POST['NewPostText']);
            $NewPostImg = $_FILES['NewPostImg']['name'];
            $NewPostImg_temp = $_FILES['NewPostImg']['tmp_name'];

            move_uploaded_file($NewPostImg_temp, "img/".$NewPostImg);

            $insert = "INSERT INTO post(category_fk, heading, text, img, date, author_fk)
                    VALUES('$NewPostCat','$NewPostHeading','$NewPostText','$NewPostImg','$today','$admin_id')";
            echo $insert;
            mysqli_query($conn, $insert);
        }
    ?>

    
</body>
</html>