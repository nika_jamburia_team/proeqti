<?php 
    if (!isset($_SESSION['admin'])){
        header("Location:../index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="ProfileDisplays/displayStyle.css">
</head>
<body>
    <div style="text-align:left;">
        <a class="UserLink NewPostBtn" href="?action=NewProduct">Add new Product</a>
    </div>
    
    <table cellspacing=0 border=1>
        <tr>
            <td class="head">ID</td>
            <td class="head">Name</td>
            <td class="head">Category</td>
            <td class="head">Main Image</td>
            <td class="head">Price</td>
            <td class="head">Products page</td>
            <td class="head"></td>
            <td class="head"></td>    
        </tr>

        <?php
            $select = "SELECT * FROM products";
            $result = mysqli_query($conn, $select);

            while ($arr=mysqli_fetch_assoc($result)){
                $tr = "
                <tr>
                    <td>".$arr['id_pr']."</td>
                    <td>".$arr['name']."</td>
                    <td>".$arr['category']."</td>
                    <td> <img style='width:inherit; height:inherit;' src='img/".$arr['mainImg']."'></td>
                    <td>".$arr['price']."$</td>
                    <td><a href='ProductPage.php?ProductId=".$arr['id_pr']."'>View page</a></td>
                    <td><a href='admin/adminQuery.php?DeleteProductId=".$arr['id_pr']."'>Delete</a></td>
                    <td><a href='adminpanel.php?action=UpdateProduct&UpdateProductId=".$arr['id_pr']."'>Update</a></td>
                </tr>
                ";
                echo $tr;
            }
        ?>

    </table>

</body>
</html>