<?php 
    if (!isset($_SESSION['admin'])){
        header("Location:../index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="ProfileDisplays/displayStyle.css">
</head>
<body>
    <form action="" method="post" enctype="multipart/form-data">
        <p>Category:</p>
        <select name="NewProductCat" id="" style="width:200px;">
            <option value="Hunting Weapons">Hunting Weapons</option>
            <option value="Personal Defence">Personal Defence</option>
            <option value="Other">Other</option>
        </select>
        <p>Name:</p>
        <input type="text" name="NewProductName" style="width: 40%;" maxlength="50" required >
        <p>Price:</p>
        <input type="number" name="NewProductPrice" style="width:40%;height:40px;" maxlength="50" required >
        <p>Manufacturer:</p>
        <input type="text" name="NewProductMan" style="width:40%;height:40px;" maxlength="50" required >
        <p>Caliber:</p>
        <input type="text" name="NewProductCal" style="width:40%;height:40px;" maxlength="50" required >
        <p>Condition:</p>
        <input type="text" name="NewProductCond" style="width:40%;height:40px;" maxlength="50" required >
        <p>Weight:</p>
        <input type="number" name="NewProductWeight" style="width:40%;height:40px;" maxlength="50" required >
        <div style="width:auto;height:auto;">
            <p>Main Image:</p>
            <input type="file" name="NewProductMainImg" required>
            <p>Image 1:</p>
            <input type="file" name="NewProductImg1">
            <p>Image 2:</p>
            <input type="file" name="NewProductImg2">
            <p>Image 3:</p>
            <input type="file" name="NewProductImg3">
            <p>Image 4:</p>
            <input type="file" name="NewProductImg4">
        </div>
        
        <br>
        <input type="submit" name="NewProductIn" value="Add Product">
    </form>

    <?php
        if(isset($_POST['NewProductIn'])){
            $NewProductCat = $_POST['NewProductCat'];
            $NewProductName = $_POST['NewProductName'];
            $NewProductPrice = $_POST['NewProductPrice'];
            $NewProductMan = $_POST['NewProductMan'];
            $NewProductCal = $_POST['NewProductCal'];
            $NewProductCond = $_POST['NewProductCond'];
            $NewProductWeight = $_POST['NewProductWeight'];

            $NewProductMainImg = $_FILES['NewProductMainImg']['name'];
            $NewProductMainImg_temp = $_FILES['NewProductMainImg']['tmp_name'];
            move_uploaded_file($NewProductMainImg_temp, "img/".$NewProductMainImg);

            $NewProductImg1 = $_FILES['NewProductImg1']['name'];
            $NewProductImg1_temp = $_FILES['NewProductImg1']['tmp_name'];

            $NewProductImg2 = $_FILES['NewProductImg2']['name'];
            $NewProductImg2_temp = $_FILES['NewProductImg2']['tmp_name'];

            $NewProductImg3 = $_FILES['NewProductImg3']['name'];
            $NewProductImg3_temp = $_FILES['NewProductImg3']['tmp_name'];

            $NewProductImg4 = $_FILES['NewProductImg4']['name'];
            $NewProductImg4_temp = $_FILES['NewProductImg4']['tmp_name'];

            $insert = "INSERT INTO products(price, name, category, caliber, conditionn, manufacturer, weight, mainImg)
                VALUES('$NewProductPrice','$NewProductName','$NewProductCat','$NewProductCal','$NewProductCond','$NewProductMan','$NewProductWeight','$NewProductMainImg')";
            mysqli_query($conn, $insert);

            if($NewProductImg1 != ""){
                move_uploaded_file($NewProductImg1_temp, "img/".$NewProductImg1);
                $update = "UPDATE products SET img1 = '$NewProductImg1' WHERE name = '$NewProductName'";
                mysqli_query($conn, $update);
                echo $update;
            }
            if($NewProductImg2 != ""){
                move_uploaded_file($NewProductImg2_temp, "img/".$NewProductImg2);
                $update = "UPDATE products SET img1 = '$NewProductImg2' WHERE name = '$NewProductName'";
                mysqli_query($conn, $update);
            }
            if($NewProductImg3 != ""){
                move_uploaded_file($NewProductImg3_temp, "img/".$NewProductImg3);
                $update = "UPDATE products SET img1 = '$NewProductImg3' WHERE name = '$NewProductName'";
                mysqli_query($conn, $update);
            }
            if($NewProductImg4 != ""){
                move_uploaded_file($NewProductImg4_temp, "img/".$NewProductImg4);
                $update = "UPDATE products SET img1 = '$NewProductImg4' WHERE name = '$NewProductName'";
                mysqli_query($conn, $update);
            }
            header("Location:adminpanel.php?action=products");
        }
    ?>

    
</body>
</html>