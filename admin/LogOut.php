<?php 
    session_start();
    include "db.php";

    if (!isset($_SESSION['admin'])){
        header("Location:../index.php");
    }
    else{
        $_SESSION['admin'] = NULL;
        $_SESSION['admin_id'] = NULL;
        header("Location:../index.php");
    }
?>