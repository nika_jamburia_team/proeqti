<?php 
    if (!isset($_SESSION['admin'])){
        header("Location:../index.php");
    }

    $UpdateProductId = $_GET['UpdateProductId'];
    $select = "SELECT * FROM products WHERE id_pr = '$UpdateProductId'";
    $result = mysqli_query($conn, $select);
    $arr = mysqli_fetch_assoc($result);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="ProfileDisplays/displayStyle.css">
</head>
<body>
    <form action="" method="post" enctype="multipart/form-data">
        <p>Category:</p>
        <input type="text" style="width:200px;" disabled="true" value="<?php echo $arr['category'] ?>">
        <select name="NewProductCat" id="" style="width:200px;">
            <option value="Hunting Weapons">Hunting Weapons</option>
            <option value="Personal Defence">Personal Defence</option>
            <option value="Other">Other</option>
        </select>
        <p>Name:</p>
        <input type="text" name="NewProductName" value="<?php echo $arr['name'] ?>" style="width: 40%;" maxlength="50">
        <p>Price:</p>
        <input type="number" name="NewProductPrice" value="<?php echo $arr['price'] ?>" style="width:40%;height:40px;" maxlength="50">
        <p>Manufacturer:</p>
        <input type="text" name="NewProductMan" value="<?php echo $arr['manufacturer'] ?>" style="width:40%;height:40px;" maxlength="50">
        <p>Caliber:</p>
        <input type="text" name="NewProductCal" value="<?php echo $arr['caliber'] ?>" style="width:40%;height:40px;" maxlength="50">
        <p>Condition:</p>
        <input type="text" name="NewProductCond" value="<?php echo $arr['conditionn'] ?>" style="width:40%;height:40px;" maxlength="50">
        <p>Weight:</p>
        <input type="number" name="NewProductWeight" value="<?php echo $arr['weight'] ?>" style="width:40%;height:40px;" maxlength="50">
        <div style="width:auto;height:auto;">
            <p>Main Image:</p>
            <input type="file" name="NewProductMainImg">
            <img width="150" height="50" src="img/<?php echo $arr['mainImg'] ?>" alt="">
            <p>Image 1:</p>
            <input type="file" name="NewProductImg1">
            <img width="150" height="50" src="img/<?php echo $arr['img1'] ?>" alt="">
            <p>Image 2:</p>
            <input type="file" name="NewProductImg2">
            <img width="150" height="50" src="img/<?php echo $arr['img2'] ?>" alt="">
            <p>Image 3:</p>
            <input type="file" name="NewProductImg3">
            <img width="150" height="50" src="img/<?php echo $arr['img3'] ?>" alt="">
            <p>Image 4:</p>
            <input type="file" name="NewProductImg4">
            <img width="150" height="50" src="img/<?php echo $arr['img4'] ?>" alt="">
        </div>
        
        <br>
        <input type="submit" name="NewProductIn" value="Edit Product">
    </form>

    <?php
        if(isset($_POST['NewProductIn'])){
            $NewProductCat = $_POST['NewProductCat'];
            $NewProductName = $_POST['NewProductName'];
            $NewProductPrice = $_POST['NewProductPrice'];
            $NewProductMan = $_POST['NewProductMan'];
            $NewProductCal = $_POST['NewProductCal'];
            $NewProductCond = $_POST['NewProductCond'];
            $NewProductWeight = $_POST['NewProductWeight'];

            $NewProductMainImg = $_FILES['NewProductMainImg']['name'];
            $NewProductMainImg_temp = $_FILES['NewProductMainImg']['tmp_name'];

            $NewProductImg1 = $_FILES['NewProductImg1']['name'];
            $NewProductImg1_temp = $_FILES['NewProductImg1']['tmp_name'];

            $NewProductImg2 = $_FILES['NewProductImg2']['name'];
            $NewProductImg2_temp = $_FILES['NewProductImg2']['tmp_name'];

            $NewProductImg3 = $_FILES['NewProductImg3']['name'];
            $NewProductImg3_temp = $_FILES['NewProductImg3']['tmp_name'];

            $NewProductImg4 = $_FILES['NewProductImg4']['name'];
            $NewProductImg4_temp = $_FILES['NewProductImg4']['tmp_name'];

            if($NewProductCat != ""){
                $update = "UPDATE products SET category = '$NewProductCat' WHERE id_pr = '$UpdateProductId'";
                mysqli_query($conn, $update);
            }
            if($NewProductName != ""){
                $update = "UPDATE products SET name = '$NewProductName' WHERE id_pr = '$UpdateProductId'";
                mysqli_query($conn, $update);
            }
            if($NewProductPrice != ""){
                $update = "UPDATE products SET price = '$NewProductPrice' WHERE id_pr = '$UpdateProductId'";
                mysqli_query($conn, $update);
            }
            if($NewProductMan != ""){
                $update = "UPDATE products SET manufacturer = '$NewProductMan' WHERE id_pr = '$UpdateProductId'";
                mysqli_query($conn, $update);
            }
            if($NewProductCal != ""){
                $update = "UPDATE products SET caliber = '$NewProductCal' WHERE id_pr = '$UpdateProductId'";
                mysqli_query($conn, $update);
            }
            if($NewProductCond != ""){
                $update = "UPDATE products SET condition = '$NewProductCond' WHERE id_pr = '$UpdateProductId'";
                mysqli_query($conn, $update);
            }
            if($NewProductWeight != ""){
                $update = "UPDATE products SET weight = '$NewProductWeight' WHERE id_pr = '$UpdateProductId'";
                mysqli_query($conn, $update);
            }
            

            if($NewProductMainImg != ""){
                move_uploaded_file($NewProductMainImg_temp, "img/".$NewProductMainImg);
                $update = "UPDATE products SET mainImg = '$NewProductMainImg' WHERE id_pr = '$UpdateProductId'";
                mysqli_query($conn, $update);
                echo $update;
            }
            if($NewProductImg1 != ""){
                move_uploaded_file($NewProductImg1_temp, "img/".$NewProductImg1);
                $update = "UPDATE products SET img1 = '$NewProductImg1' WHERE id_pr = '$UpdateProductId'";
                mysqli_query($conn, $update);
                echo $update;
            }
            if($NewProductImg2 != ""){
                move_uploaded_file($NewProductImg2_temp, "img/".$NewProductImg2);
                $update = "UPDATE products SET img2 = '$NewProductImg2' WHERE id_pr = '$UpdateProductId'";
                mysqli_query($conn, $update);
            }
            if($NewProductImg3 != ""){
                move_uploaded_file($NewProductImg3_temp, "img/".$NewProductImg3);
                $update = "UPDATE products SET img3 = '$NewProductImg3' WHERE id_pr = '$UpdateProductId'";
                mysqli_query($conn, $update);
            }
            if($NewProductImg4 != ""){
                move_uploaded_file($NewProductImg4_temp, "img/".$NewProductImg4);
                $update = "UPDATE products SET img4 = '$NewProductImg4' WHERE id_pr = '$UpdateProductId'";
                mysqli_query($conn, $update);
            }
            echo "<meta http-equiv='refresh' content='0'>";
        }
    ?>
    
</body>
</html>