<?php 
    if (!isset($_SESSION['admin'])){
        header("Location:../index.php");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="ProfileDisplays/displayStyle.css">
</head>
<body>

    <table cellspacing=0 border=1>
        <tr>
            <td class="head">ID</td>
            <td class="head">Text</td>
            <td class="head">Post</td>
            <td class="head">Date</td>
            <td class="head"></td>  
        </tr>

        <?php
            $select = "SELECT * FROM comments";
            $result = mysqli_query($conn, $select);

            while ($arr=mysqli_fetch_assoc($result)){
                $Post_Id = $arr['post_fk'];
                $selectPost = "SELECT * FROM post WHERE id_p = '$Post_Id'";
                $result2 = mysqli_query($conn, $selectPost);
                $arrP = mysqli_fetch_assoc($result2);

                $tr = "
                <tr>
                    <td> ".$arr['id_c']." </td>
                    <td>".$arr['text']."</td>
                    <td><a href='postpage.php?postID=".$arr['post_fk']."'>".$arrP['heading']."</a></td>
                    <td>".$arr['date']."</td>
                    <td><a href='admin/adminQuery.php?DeleteCommId=".$arr['id_c']."'>Delete</a></td>
                </tr>
                ";
                echo $tr;
            }
        ?>

    </table>

</body>
</html>