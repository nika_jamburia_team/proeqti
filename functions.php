<?php 
    function Generate_keyy_sql($tablename){
        require "db.php";
        $reserved = true;

        function generate(){
            $testKey = "";
            for ($i=0;$i<5;$i++){
                $rand = rand(0,9);
                $testKey .= $rand;
            }
            return $testKey;
        }

        $select = "SELECT * FROM $tablename";
        $result = mysqli_query($conn, $select);
        $tk = generate();
        while($arr = mysqli_fetch_assoc($result)){
            $KeyFromTable = $arr['keyy'];
            if ($KeyFromTable == $tk){
                mysqli_data_seek($result,0);
                $tk = generate();
            }
        }
        $key = $tk;
        return $key;
    }

?>