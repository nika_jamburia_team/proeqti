<?php 
    session_start();
    include "db.php";
    if (!isset($_SESSION['admin'])){
        header("Location:index.php");
    }
?>
<!DOCTYPE html>
<html lang="en" dir="ltr"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <link rel="stylesheet" href="style.css">
    <title>No-Legalo | Admin Panel</title>
  </head>
  <body>
  
      <div class="NameAndLozung">
        <span class="name">No- <span style="color: #333333;">Legalo</span> </span>
        <br>
        <span class="lozung">Buy Here - Die There</span>
      </div>

     <div id="header">
       <ul>
          <li><a id="MainPageLink" class="active" href="index.php">Home</a></li>
          <li><a id="ShopLink" href="ShopPage.php">Sales</a></li>
          <li><a id="ProfileLink" href="ProfilePage.php">Profile</a></li>
          <li class="Cart" style="float:right"><a id="CartLink" href="Cart.php" >Cart</a></li>          
          <li class="Login" style="float:right" onclick="document.getElementById(&#39;id01&#39;).style.display=&#39;block&#39;"><a id="LoginLink" href="LogIn.html">Log In / Out</a></li>
          <li class="GEO" onclick="changelagunge()" style="float:right"> <a id="LanguageButtonText">GEO</a> </li>
          <li class="Moreoptions" onclick="ShowMenuOptions()" style="float:right"> <a>|||</a> </li>
       </ul>
       </div>
       <div id="MoreoptionsBox" onmouseleave="HideMenuOptions()">
          <div class="option" onclick="changelagunge()" id="LanguageButtonText">GEO</div>
          <div><a href="LogIn.html" class="option">Log In</a></div>
          <div><a href="" class="option">Cart</a></div>
      </div>
     
     <br>
     <div class="recbox">
        <div class="recleft"> <img class="ad" src="img/rec1.gif" alt=""> </div>
        <div class="recright"> <img class="ad" src="img/rec1.gif" alt=""> </div>
     </div>

     <div class="rectop"> <img class="ad2" src="img/rec2.jpg" alt=""> </div>

     <div class="main"> 
        <div class="UserImgBox">
            <img style="width:inherit; height:200px" src="img/adminimg.png">
        </div>
        <div class="UserInfo">
            <h2>
                Administrator: <?php echo $_SESSION['admin']; ?>
            </h2>
            <a class="UserLink" href="?action=posts">Posts</a> 
            <a class="UserLink" href="?action=comments">Comments</a>
            <a class="UserLink" href="?action=products">Products</a>
            <a class="UserLink" href="?action=orders">Orders</a>
            <a class="UserLink" href="?action=edit">Edit Admin</a>
            <a class="UserLink" href="admin/LogOut.php">Log Out</a>   
        </div>
        <div class="UserDisplay">
            <?php 
                if(isset($_GET['action'])){
                    switch ($_GET['action']){
                        case 'edit';
                            include "admin/edit.php";
                            break;
                        case 'posts';
                            include "admin/posts.php";
                            break;    
                        case 'comments';
                            include "admin/comments.php";
                            break;
                        case 'products';
                            include "admin/products.php";
                            break;
                        case 'orders';
                            include "admin/orders.php";
                            break;
                        case 'NewPost';
                            include "admin/NewAdminPost.php";
                            break;  
                        case 'UpdatePost';
                            include "admin/UpdatePost.php";
                            break;  
                        case 'NewProduct';
                            include "admin/NewProduct.php";
                            break;
                        case 'UpdateProduct';
                            include "admin/UpdateProduct.php";
                            break;         
                    }
                }
            ?>
        </div>
        
     </div>
       

     <div class="adtop" style="margin-top:20px;"> <img class="ad2" src="img/rec2.jpg" alt=""> </div>
     <script src="main.js"></script>


  

</body></html>