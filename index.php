<?php
  session_start();
  include "db.php";

  if(isset($_COOKIE['user_id'])){
    $_SESSION['user_id'] = $_COOKIE['user_id'];
    $_SESSION['Email'] = $_COOKIE['Email'];
    $_SESSION['Img'] = $_COOKIE['Img'];
  }
?>

<!DOCTYPE html>
<html lang="en" dir="ltr"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <link rel="stylesheet" href="style.css">
    <title>No-Legalo | Main Page</title>
  </head>
  <body>
  
      <div class="NameAndLozung">
        <span class="name">No- <span style="color: #333333;">Legalo</span> </span>
        <br>
        <span class="lozung">Buy Here - Die There</span>
      </div>

     <div id="header">
       <ul>
          <li><a id="MainPageLink" class="active" href="index.php">Home</a></li>
          <li><a id="ShopLink" href="ShopPage.php">Sales</a></li>
          <li><a id="ProfileLink" href="ProfilePage.php">Profile</a></li>
          <li class="Cart" style="float:right"><a id="CartLink" href="Cart.php" >Cart</a></li>
          <li class="Login" style="float:right" onclick="document.getElementById(&#39;id01&#39;).style.display=&#39;block&#39;"><a id="LoginLink" href="LogIn.html">Log In / Out</a></li>
          <li class="GEO" onclick="changelagunge()" style="float:right"> <a id="LanguageButtonText">GEO</a> </li>
          <li class="Moreoptions" onclick="ShowMenuOptions()" style="float:right"> <a>|||</a> </li>
       </ul>
       </div>
       <div id="MoreoptionsBox" onmouseleave="HideMenuOptions()">
          <div class="option" onclick="changelagunge()" id="LanguageButtonText">GEO</div>
          <div><a href="LogIn.html" class="option">Log In</a></div>
          <div><a href="" class="option">Cart</a></div>
      </div>
     
     <br>
     <div class="recbox">
        <div class="recleft"> <img class="ad" src="img/rec1.gif" alt=""> </div>
        <div class="recright"> <img class="ad" src="img/rec1.gif" alt=""> </div>
     </div>

     <div class="rectop"> <img class="ad2" src="img/rec2.jpg" alt=""> </div>

     <div class="main">
       <div class="categorybox">
         <h3 style="margin-left:20px;">Categories:</h3>
        <ul class="categoryul">
          <li class="categoryli"><a class="categorya" href=""> <span class="categoryspan">All Posts </span></a></li>
          <li class="categoryli"><a class="categorya" href="Category.php?cat=1"><span class="categoryspan">News</span></a></li>
          <li class="categoryli"><a class="categorya" href="Category.php?cat=2"><span class="categoryspan">User posts</span></a></li>
          <li class="categoryli" style="border-bottom-radius:10px;"><a class="categorya" style="border-bottom-left-radius:10px;border-bottom-right-radius:10px;" href="Category.php?cat=3"><span class="categoryspan">Other</span></a></li>
        </ul>
       </div>

       <?php
          $select = "SELECT * FROM post ORDER BY date DESC";
          $result = mysqli_query($conn, $select);
          while($arr = mysqli_fetch_assoc($result)){
            $post_id = $arr['id_p'];
            $selectComs = "SELECT * FROM comments WHERE post_fk = '$post_id'";
            $resultComs = mysqli_query($conn, $selectComs);
            $commentCount = 0;
            while($arrComs = mysqli_fetch_assoc($resultComs)){
              $commentCount++;
            }

            $object = "
            <div class='postbox'>
              <div class='postimgbox'><img src='img/".$arr['img']."' style='width:inherit; height:inherit'></div>
              <div class='posttextbox'>
                <div class='postheading'>". $arr['heading'] ."</div>
                <hr style='margin-top:5px;margin-bottom:5px;'>
                <div class='postshorttext'> ". $arr['text'] ." <tr>  </div>
                <div style='margin-top:5px;float:left;'>
                  <small style='color:grey;'> ". $arr['date'] ." </small>
                  <small style='color:grey;margin-left:10px;'> Views: ". $arr['viewCount'] ." </small>
                  <small style='color:grey;margin-left:10px;'> Comments: ". $commentCount ." </small>
                  <small style='color:grey;margin-left:10px;'> Likes: ". $arr['likes'] ." </small>
                </div>
              <span><a class='postmore' href='postpage.php?postID=". $arr['id_p'] ."'>More >></a></span>
              </div>
            </div>
            ";
            echo $object;
          }
       ?>
       </div>

     <div class="adtop" style="margin-top:20px;"> <img class="ad2" src="img/rec2.jpg" alt=""> </div>
     <script src="main.js"></script>


  

</body></html>