<?php
  session_start();
  include "db.php";
  if (isset($_SESSION['user_id'])){
    $user_id = $_SESSION['user_id'];
    $UserEmail = $_SESSION['Email'];
  }

  $postId = $_GET['postID'];
  $select = "SELECT * From post WHERE id_p='$postId'";
  $result = mysqli_query($conn, $select);
  $arr = mysqli_fetch_assoc($result);

  if(isset($_GET['action'])){
    if($_GET['action'] == "like"){
      if(isset( $_SESSION['user_id'])){
        $updateLikes = "UPDATE post SET likes = likes+1 WHERE id_p = '$postId'";
        mysqli_query($conn, $updateLikes);

        $insertLikes = "INSERT INTO likes(user_fk, post_fk) VALUES ('$user_id', '$postId')";
        mysqli_query($conn, $insertLikes);
        header("Location:postpage.php?postID=".$postId);
      }
    }
    if($_GET['action'] == "unlike"){
      $updateLikes = "UPDATE post SET likes = likes-1 WHERE id_p = '$postId'";
      mysqli_query($conn, $updateLikes);

      $deleteLikes = "DELETE FROM likes WHERE user_fk = '$user_id' AND post_fk = '$postId'";
      mysqli_query($conn, $deleteLikes);
      header("Location:postpage.php?postID=".$postId);
    }
  }


  $author = $arr['author_fk'];
  $selectAuthor = "SELECT id_u, Email FROM users where id_u = $author";
  $resultAuthor = mysqli_query($conn, $selectAuthor);
  $arrA = mysqli_fetch_assoc($resultAuthor);

  if(isset($_SESSION['user_id'])){
    $liked = false;
    $selectLikes = "SELECT * FROM likes WHERE post_fk = '$postId' ";
    $resultLikes = mysqli_query($conn, $selectLikes);
    while($arrL = mysqli_fetch_assoc($resultLikes)){
      if($arrL['user_fk'] == $user_id){
        $liked = true;
        break;
      }
    } 
  }

  $CountView = true;
  if(isset($_SESSION['user_id'])){
    if($_SESSION['user_id'] == $arrA['id_u']){
        $CountView = false;
    }
  }
  if($CountView){
    $update = "UPDATE post SET viewCount = viewCount+1 WHERE id_p = '$postId'";
    mysqli_query($conn, $update);
  }

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title>No-Legalo | Post</title>
  </head>
  <body>
      <div class="NameAndLozung">
        <span class="name">No- <span style="color: #333333;" >Legalo</span> </span>
        <br>
        <span class="lozung">Buy Here - Die There</span>
      </div>

     <div id="header">
       <ul>
          <li><a id="MainPageLink" class="active" href="index.php">Home</a></li>
          <li><a id="ShopLink" href="ShopPage.php">Sales</a></li>
          <li><a id="ProfileLink" href="ProfilePage.php">Profile</a></li>
          <li class="Cart" style="float:right"><a id="CartLink" href="Cart.php" >Cart</a></li>          
          <li class="Login" style="float:right"><a id="LoginLink" onclick="document.getElementById('id01').style.display='block'"  href="LogIn.html">Log In / Out</a></li>
          <li class="GEO" onclick="changelagunge()" style="float:right"> <a id="LanguageButtonText">GEO</a> </li>
          <li class="Moreoptions" onclick="ShowMenuOptions()" style="float:right"> <a>|||</a> </li>
       </ul>
       </div>
       <div id="MoreoptionsBox" onmouseleave="HideMenuOptions()">
         <div class="option" onclick="changelagunge()" id="LanguageButtonText" >GEO</div>
         <div ><a href="LogIn.html" class="option">Log In</a></div>
         <div><a href="" class="option">Cart</a></div>
     </div>
     </div>
     <br>
     <div class="recbox">
        <div class="recleft"> <img class="ad" src="img/rec1.gif" alt=""> </div>
        <div class="recright"> <img class="ad" src="img/rec1.gif" alt=""> </div>
     </div>

     <div class="rectop"> <img class="ad2" src="img/rec2.jpg" alt=""> </div>

     <div class="main">
      <a class="backBtn" href="index.php"> << Back To Main page </a>
      <div class="postpageimg"><img style="width:inherit; height:inherit" src="img/<?php echo $arr['img']; ?>" alt=""></div>

      <div class="postpageheading">
        <h2 style="padding:0px;margin:0px;"> <?php echo $arr['heading']; ?> </h2>
        <span class="authorbox"><?php echo $arr['date']; ?> </span>
        <hr>
      </div>
      <div class="postpagetext">
        <?php echo $arr['text']; ?>
        <br>
        <span style="color:white; float:right"><i>Author: <?php echo $arrA["Email"] ?> </i></span>
        <?php  
          if(isset($liked)){
            if($liked){
              echo "<a class='more' style='margin-top:30px;float:left;' href='postpage.php?postID=".$postId."&action=unlike'> Unlike </a>
              <span class='badge'>".$arr['likes']."</span>
              ";
            }
            else{
              echo "<a class='more' style='margin-top:30px;float:left;' href='postpage.php?postID=".$postId."&action=like'> Like </a>
              <span class='badge'>".$arr['likes']."</span>
              ";
              
            }
          }
        ?>
      </div>
      <hr class="idk" style="border:1px solid #464646;">
      <h2 class="idk" style="color:white">Comments:</h2>

      <div class="idk commentsbox">

      <?php
        $select = "SELECT * FROM comments where post_fk = '$postId'";
        $result = mysqli_query($conn, $select);        
        while ($arr = mysqli_fetch_assoc($result)){
          $author = $arr['author_fk'];
          $selectAuthor = "SELECT Email, Img FROM users where id_u = $author";
          $resultAuthor = mysqli_query($conn, $selectAuthor);
          $arrA = mysqli_fetch_assoc($resultAuthor);

          $object = "
          <div class='commentbody'>
          <div class='commentimg'><img style='width:inherit; height:inherit;' src='img/".$arrA['Img']."'></div>
          <div class='commenttext'>
            <span style='font-size:20px;'>".$arrA['Email']."</span>
            <span style='color:grey;float:right'>".$arr['date']."</span>
            <hr>
            <span> ".$arr['text']." </span>
          </div>
        </div>
          ";
          echo $object;
        }
      ?>
      <br>
      <hr style="border:1px solid #464646;">

      <form style ="float:left;text-align:left;" action="" method="post">
        <textarea class="NewCommtxt" name="NewCommText" cols="80" rows="7" maxlength="200"></textarea>
        <br>
        <input class="NewCommBtn" name="NewCommIn" type="submit" value="Add comment">
      </form>
      <?php
        if(isset($_POST['NewCommIn'])){
          if($_POST['NewCommText'] != ""){
            $Commtxt = $_POST['NewCommText'];
            $Today = date("Y-m-d");
            $insert = "INSERT INTO comments(author_fk, post_fk, date, text) 
                        VALUES('$user_id', '$postId', '$Today', '$Commtxt')";
            mysqli_query($conn, $insert);
            echo "<meta http-equiv='refresh' content='0'>";
          }
        }
      ?>
        
    </div>
    </div>

     <div class="adtop" style="margin-top:20px;"> <img class="ad2" src="img/rec2.jpg" alt=""> </div>
     <script src="main.js"></script>
  </body>
</html>
