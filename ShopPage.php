<?php
 include "db.php";
 $select = "SELECT * FROM products ORDER BY id_pr DESC";
 $result = mysqli_query($conn, $select);
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="ShopStyle.css">
    <title>No-Legalo | Shop</title>
  </head>
  <body>
      <div class="NameAndLozung">
        <span class="name">No- <span style="color: #333333;" >Legalo</span> </span>
        <br>
        <span class="lozung">Buy Here - Die There</span>
      </div>

     <div id="header">
       <ul>
          <li><a id="MainPageLink" class="active" href="index.php">Home</a></li>
          <li><a id="ShopLink" href="ShopPage.php">Sales</a></li>
          <li><a id="ProfileLink" href="ProfilePage.php">Profile</a></li>
          <li class="Cart" style="float:right"><a id="CartLink" href="Cart.php" >Cart</a></li>          
          <li class="Login" style="float:right"><a id="LoginLink" onclick="document.getElementById('id01').style.display='block'"  href="LogIn.html">Log In / Out</a></li>
          <li class="GEO" onclick="changelagunge()" style="float:right"> <a id="LanguageButtonText">GEO</a> </li>
          <li class="Moreoptions" onclick="ShowMenuOptions()" style="float:right"> <a>|||</a> </li>
       </ul>
       </div>
       <div id="MoreoptionsBox" onmouseleave="HideMenuOptions()">
         <div class="option" onclick="changelagunge()" id="LanguageButtonText" >GEO</div>
         <div ><a href="LogIn.html" class="option">Log In</a></div>
         <div><a href="" class="option">Cart</a></div>
     </div>
     </div>
     <br>
     <div class="recbox">
        <div class="recleft"> <img class="ad" src="img/rec1.gif" alt=""> </div>
        <div class="recright"> <img class="ad" src="img/rec1.gif" alt=""> </div>
     </div>

     <div class="adtop"> <img class="ad2" src="img/ad2.jpg" alt=""> </div>

     <div class="main2">
       <form action="shopFilter.php?type=filter" method="post">
        <div class="ShopMenu">
            <span>Filter:</span> <br>
            <div class="FilterOption">Manufacturer: <br>
              <input type="text" name="manufacturer" style="height:20px;">
          </div>
            <div class="FilterOption">Category: <br>
              <select name="Category" style="height:25px; width:100%;margin-bottom:10px;">
                  <option value=""></option>
                  <option value="Hunting Weapons">Hunting Weapons</option>
                  <option value="Personal Defence">Personal Defence</option>
                  <option value="Other">Other</option>
              </select>
          </div>
            <div class="FilterOption">Condition: <br>
              <input type="radio" name="cond" value="New" id="">-New
              <input type="radio" name="cond" value="Used" id="">-Used
            </div>
            <div class="FilterOption">Price: <br>
                From- <input name="PriceFrom" style="width:45px;height:15px;margin-bottom:10px;" type="number">
                To- <input name="PriceTo" style="width:45px; height:15px;margin-bottom:10px;" type="number">
            </div>
            <div class="Apply">
              <input type="submit" value="Apply Filter" name="FilterIn" style="width:100px;border:0px; height:30px;margin:0px;background: #333333;color:white;border: 0px;">
            </div>
          </form>
        </div>

        <div class="SearchBar">
          <form action="shopFilter.php?type=search" method="post">
              <input type="text" name="search" placeholder="Search..." class="searchField" style="height:50px;border-top-left-radius:5px;border-bottom-left-radius:5px;float:left;width:80%;margin:0px;">
              <input type="submit" name="searchIn" value="Search" style="float:right;width:20%;height:50px;border: 0.5px solid #464646;border-top-right-radius:5px;border-bottom-right-radius:5px;background-color:#252526; color:white">
            </form>
        </div>
        <div class="ShopContent">

        <?php
          while($arr=mysqli_fetch_assoc($result)){
            $object = "
            <div class='box'>
            <div class='imageBox'> <img class='image' src='img/".$arr['mainImg']."'> </div>
            <a id='moreSpan' class='more' href='ProductPage.php?ProductId=".$arr['id_pr']."'> More >></a>
            <div class='boxContent'> ".$arr['name']." <br>
              <span class='priceSpan' style='font-weight:bold;'>Price: </span>
               <span class='price'>".$arr['price']." $</span>
             </div> <br>
            </div>
            ";
            echo $object;
          }
        ?>
     </div>

     <div class="adtop" style="margin-top:20px;"> <img class="ad2" src="img/ad2.jpg" alt=""> </div>
     <script src="main.js"></script>
  </body>
</html>
