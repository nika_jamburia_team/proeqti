<?php 
    class User{
        private $mail;
        private $password;
        private $repassword;
        public $error = false;
        public $messege = "";

        public function __construct($Email, $Password, $Repassword){
            $this->mail = $Email;
            $this->password = $Password;
            $this->repassword = $Repassword;
            $this->ValidateEmail();
            $this->MatchPasswords();
            $this->PasswordLength();
            $this->Search();
        } 

        private function ValidateEmail(){
            if (!filter_var($this->mail, FILTER_VALIDATE_EMAIL)){
                $this->messege .= "Invalid Email <br>";
                $this->error = true;
              }
        }

        private function MatchPasswords(){
            if ($this->password != $this->repassword){
                $this->messege .= "Passwords do not Match <br>";
                $this->error = true;
            }
        }
        private function PasswordLength(){
            if (strlen($this->password) < 4){
                $this->messege .= "Password must be at least 4 symbols long <br>";
                $this->error = true;
            }
        }
        private function Search(){
            include "db.php";
            $select = "SELECT * From users";
            $result = mysqli_query($conn, $select);
            while($arr = mysqli_fetch_assoc($result)){
                if($this->mail === $arr['Email']){
                    $this->messege .= "This Email address already exists <br>";
                    $this->error = true;
                    break;
                }
            }
        }
    }
?>