<?php 
    include "db.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="ProfileDisplays/displayStyle.css">
</head>
<body>
    <div style="text-align:left;">
        <a class="UserLink NewPostBtn" href="?action=NewPost">Add new post</a>
    </div>
    
    <table cellspacing=0 border=1>
        <tr>
            <td class="head">Heading</td>
            <td class="head">Category</td>
            <td class="head">Image</td>
            <td class="head">Date</td>
            <td class="head">Post's page</td>
            <td class="head"></td>
            <td class="head"></td>    
        </tr>

        <?php
            $userId = $_SESSION['user_id'];
            $select = "SELECT * FROM post WHERE author_fk = '$userId'";
            $result = mysqli_query($conn, $select);

            while ($arr=mysqli_fetch_assoc($result)){
                $Cat_Id = $arr['category_fk'];
                $selectPost = "SELECT * FROM categories WHERE id_ca = '$Cat_Id'";
                $result2 = mysqli_query($conn, $selectPost);
                $arrC = mysqli_fetch_assoc($result2);

                $tr = "
                <tr>
                    <td>".$arr['heading']."</td>
                    <td>".$arrC['Name']."</td>
                    <td> <img style='width:inherit; height:inherit;' src='img/".$arr['img']."'></td>
                    <td>".$arr['date']."</td>
                    <td><a href='postpage.php?postID=".$arr['id_p']."'>View page</a></td>
                    <td><a href='ProfileDisplays/userQuery.php?DeletePostId=".$arr['id_p']."'>Delete</a></td>
                    <td><a href='ProfilePage.php?action=UpdatePost&UpdatePostId=".$arr['id_p']."'>Update</a></td>
                </tr>
                ";
                echo $tr;
            }
        ?>

    </table>

</body>
</html>