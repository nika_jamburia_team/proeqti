<?php 
    include "db.php";
    $user_id = $_SESSION['user_id'];

    $selectOrders = "SELECT * FROM orderstable WHERE user_fk = '$user_id'";
    $resultOrders = mysqli_query($conn, $selectOrders);
    $orderCounter = 0;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="ProfileDisplays/displayStyle.css">
    <title>Document</title>
</head>
<body>
    <?php while($arrOrders = mysqli_fetch_assoc($resultOrders)){
        $orderCounter++;
     ?>
    <div class="orderlabel">Order <?php echo $orderCounter ?></div>
    <div class="orderbox">
    <?php 
        $order_id = $arrOrders['id_ord'];
        $selectOrderItems = "SELECT * FROM ordersproducts WHERE order_fk = '$order_id'";
        //echo $selectOrderItems;
        $resultOrderItems = mysqli_query($conn, $selectOrderItems);
        while($arrOrderItems = mysqli_fetch_assoc($resultOrderItems)){
            $product_id = $arrOrderItems['product_fk'];
            $selectProduct = "SELECT * FROM products WHERE id_pr = '$product_id'";
            $resultProduct = mysqli_query($conn, $selectProduct);
            //echo $selectProduct."<br>";
            $arrProduct = mysqli_fetch_assoc($resultProduct);
    ?>

    <div class='postbox' style="margin-top:10px;margin-left:auto;margin-right:auto;">
    <div class='postimgbox'><img src='img/<?php echo $arrProduct['mainImg'] ?>' style='width:inherit; height:inherit'></div>
    <div class='posttextbox'>
    <div class='postheading'><?php echo $arrProduct['name'] ?></div>
    <hr style='margin-top:5px;margin-bottom:5px;'>
    <div class='postshorttext'> <span style="color:rgb(0, 129, 28)"></span>Quantity: <?php echo $arrOrderItems['quantity'];  ?> </span> <tr>  </div>
    <span><a class='postmore cartbtn ' href='ProductPage.php?ProductId=<?php echo $arrProduct['id_pr'] ?>'>View Products page  </a></span>
    </div>
    </div>

    <?php } ?>
    <br>
    <div class="orderPrice"><span style="margin-right:130px;">Total Price: <span style="color:green;"> <?php echo $arrOrders['price']; ?>$ </span></span></div>
    </div>
    <?php } ?>
</body>
</html>