<?php 
    include "db.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="ProfileDisplays/displayStyle.css">
</head>
<body>
    <form action="" method="post" enctype="multipart/form-data">
        <div style="width:200px;height:auto;">
            <p>New Image:</p>
            <input type="file" name="NewImg" >
        </div>
        <p>New Email:</p>
        <input type="text" name="NewUserEmail">
        <p>New Password:</p>
        <input type="password" name="NewUserPass">
        <p>Repeat New Password:</p>
        <input type="password" name="NewUserRePass">
        <br>
        <input type="submit" name="UserEditIn" value="Apply Changes">
    </form>

    <?php 
        if(isset($_POST['UserEditIn'])){
            $NewImage = $_FILES['NewImg']['name'];
            $NewImageTemp = $_FILES['NewImg']['tmp_name'];
            $NewEmail = $_POST['NewUserEmail'];
            $NewPass = $_POST['NewUserPass'];
            $NewRePass = $_POST['NewUserRePass'];
            $user_id = $_SESSION['user_id'];
            $error = false;
            $errormessege = "";

            if(move_uploaded_file($NewImageTemp, "img/".$NewImage)){
                $update = "UPDATE users SET Img = '$NewImage' WHERE id_u = '$user_id'";
                mysqli_query($conn, $update);
                $_SESSION['Img'] = $NewImage;
            }

            if ($NewEmail != ""){
                if (filter_var($NewEmail, FILTER_VALIDATE_EMAIL)){
                    $update = "UPDATE users SET Email = '$NewEmail' WHERE id_u = '$user_id'";
                    mysqli_query($conn, $update);
                    $_SESSION['Email'] = $NewEmail;
                  }
            }

            if ($NewPass != ""){
                $CanChangePass = true;
                if ($NewPass != $NewRePass){
                    $errormessege .= "Passwords do not Match <br>";
                    $error = true;
                }
                else if(strlen($NewPass) < 4){
                    $errormessege .= "Password must be at least 4 symbols long <br>";
                    $error = true;
                }
                else{
                    $hashed = password_hash($NewPass, PASSWORD_DEFAULT);
                    $update = "UPDATE users SET Passsword = '$hashed' WHERE id_u = '$user_id'";
                    mysqli_query($conn, $update);
                    $_SESSION['Password'] = $NewPass;  

                }
            }

            if ($error == true){
                echo "<div style='text-align:left;width:80%; height:contain;background:#333;color:red;padding:10px;'>".$errormessege. "</div>";
            }
            else{
                header("Refresh:0");
            }

        }
    ?>

</body>
</html>