<?php
    include "db.php";
    if(isset($_GET['UpdatePostId'])){
        $UpdatePostId = $_GET['UpdatePostId'];

        $select = "SELECT * FROM post WHERE id_p = '$UpdatePostId'";
        $result = mysqli_query($conn, $select);
        $arr = mysqli_fetch_assoc($result);
        $cat = $arr['category_fk'];

        $selectCat = "SELECT Name FROM categories WHERE id_ca = '$cat'";
        $result2 = mysqli_query($conn, $selectCat);
        $arrC = mysqli_fetch_assoc($result2);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="ProfileDisplays/displayStyle.css">
    <title>Edit Post</title>
</head>
<body>

<form action="" method="post" enctype="multipart/form-data">
        <p>Category:</p>
        <input type="text" name="CurrentCat" style="width: 110px;" value=" <?php echo $arrC['Name'] ?> " disabled="true" >
        <select name="UpPostCat" placeholder="New">
            <option value="">New Category</option>
            <option value="1">News</option>
            <option value="2">User Posts</option>
            <option value="3">Other</option>
        </select>
        <p>Heading:</p>
        <input type="text" name="UpPostHeading" style="width: 40%;" maxlength="50" value="<?php echo $arr['heading'] ?>" required >
        <div style="width:auto;height:auto;">
            <p>Image:</p>
            <input type="file" name="UpPostImg">
            <img width="100" height="50" src="img/<?php echo $arr['img'] ?>" alt="">
        </div>
        <p>Main text:</p>
        <small style="color:grey">You can't type more than 5000 characters</small> <br>
        <textarea class="NewCommtxt" name="UpPostText" cols="80" rows="30" maxlength="5000" required > <?php echo $arr['text'] ?> </textarea>
        <br>
        <input type="submit" name="UpPostIn" value="Edit Post">
    </form>

    <?php
        if(isset($_POST['UpPostIn'])){
            $UpPostCat=$_POST['UpPostCat'];
            $UpPostHeading=$_POST['UpPostHeading'];
            $UpPostText=mysqli_real_escape_string($conn, $_POST['UpPostText']);
            $UpPostImg = $_FILES['UpPostImg']['name'];
            $UpPostImg_temp = $_FILES['UpPostImg']['tmp_name'];

            if ($UpPostCat != ""){
                echo "cat is set ";
                $update = "UPDATE post SET category_fk = '$UpPostCat' WHERE id_p = '$UpdatePostId'";
                mysqli_query($conn, $update);
            }
            if ($UpPostHeading != $arr['heading']){
                echo "head is set ";
                $update = "UPDATE post SET heading = '$UpPostHeading' WHERE id_p = '$UpdatePostId'";
                mysqli_query($conn, $update);
            }
            if($UpPostImg != ""){
                echo "img is set ";
                move_uploaded_file($UpPostImg_temp, "img/".$UpPostImg);
                $update = "UPDATE post SET img = '$UpPostImg' WHERE id_p = '$UpdatePostId'";
                mysqli_query($conn, $update);
            }
            if ($UpPostText != $arr['text']){
                echo "txt is set ";
                $update = "UPDATE post SET text = '$UpPostText' WHERE id_p = '$UpdatePostId'";
                mysqli_query($conn, $update);
                echo "<meta http-equiv='refresh' content='0'>";
            }
        }
    ?>
    
</body>
</html>