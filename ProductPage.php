<?php
$productId = $_GET['ProductId'];
 include "db.php";
 $select = "SELECT * FROM products where id_pr = '$productId'";
 $result = mysqli_query($conn, $select);
 $arr = mysqli_fetch_assoc($result);
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title>No-Legalo | Product page</title>
    <script src="main.js"></script>
  </head>
  <body onload="fade()" >
      <div class="NameAndLozung">
        <span class="name">No- <span style="color: #333333;" >Legalo</span> </span>
        <br>
        <span class="lozung">Buy Here - Die There</span>
      </div>

     <div id="header">
       <ul>
          <li><a id="MainPageLink" class="active" href="index.php">Home</a></li>
          <li><a id="ShopLink" href="ShopPage.php">Sales</a></li>
          <li><a id="ProfileLink" href="ProfilePage.php">Profile</a></li>
          <li class="Cart" style="float:right"><a id="CartLink" href="Cart.php" >Cart</a></li>          
          <li class="Login" style="float:right"><a id="LoginLink" onclick="document.getElementById('id01').style.display='block'"  href="LogIn.html">Log In / Out</a></li>
          <li class="GEO" onclick="changelagunge()" style="float:right"> <a id="LanguageButtonText">GEO</a> </li>
          <li class="Moreoptions" onclick="ShowMenuOptions()" style="float:right"> <a>|||</a> </li>
       </ul>
       </div>
       <div id="MoreoptionsBox" onmouseleave="HideMenuOptions()">
         <div class="option" onclick="changelagunge()" id="LanguageButtonText" >GEO</div>
         <div ><a href="LogIn.html" class="option">Log In</a></div>
         <div><a href="" class="option">Cart</a></div>
     </div>
     </div>
     <br>
     <div class="recbox">
        <div class="recleft"> <img class="ad" src="img/rec1.gif" alt=""> </div>
        <div class="recright"> <img class="ad" src="img/rec1.gif" alt=""> </div>
     </div>

     <div class="adtop"> <img class="ad2" src="img/ad2.jpg" alt=""> </div>

     <div class="main2">
        <a class="backBtn" href="ShopPage.php"> << Back To Shop </a>
        <div class="ProductPhoto" id="photo1"><img style="z-index:0;width:inherit; height:inherit;border-radius:5px;" src="img/<?php echo $arr['mainImg'] ?>"> </div>
        <div class="ProductPhoto slider-photo-hidden" id="photo2"><img style="z-index:0;width:inherit; height:inherit;border-radius:5px;" src="img/<?php echo $arr['img1'] ?>"> </div>
        <div class="ProductPhoto slider-photo-hidden" id="photo3"><img style="z-index:0;width:inherit; height:inherit;border-radius:5px;" src="img/<?php echo $arr['img2'] ?>"> </div>
        <div class="ProductPhoto slider-photo-hidden" id="photo4"><img style="z-index:0;width:inherit; height:inherit;border-radius:5px;" src="img/<?php echo $arr['img3'] ?>"> </div>
        <div class="ProductPhoto slider-photo-hidden" id="photo5"><img style="z-index:0;width:inherit; height:inherit;border-radius:5px;" src="img/<?php echo $arr['img4'] ?>"> </div>
        <div class="ProductInfo">
            <div class="ProductName"><?php echo $arr['name'] ?></div>
            <hr style="border:1px solid #464646;">
            <div class="ProductSpecs" style="margin-top:10px;">Price: <span style="font-size:18px;color:rgb(0, 129, 28)"> <?php echo $arr['price']. " "; ?>$ </span></div>
            <div class="ProductSpecs">Availability: <span style="font-size:18px;color:rgb(0, 129, 28)"> <?php echo $arr['availability']; ?> </span></div>
            <div class="ProductSpecs">Category: <span style="font-size:18px"><?php echo $arr['category']; ?></span></div>
            <div class="ProductSpecs"> Manufacturer: <span style="font-size:18px"><?php echo $arr['manufacturer']; ?></span> </div>
            <div class="ProductSpecs"> Caliber: <span style="font-size:18px"><?php echo $arr['caliber']; ?></span> </div>
            <div class="ProductSpecs"> Weight: <span style="font-size:18px"><?php echo $arr['weight']; ?></span></div>
            <div class="ProductSpecs"> Condition: <span style="font-size:18px"><?php echo $arr['conditionn']; ?></span></div>
            <form action="AddToCart.php" method="post">
            <input type="hidden" name="product_id" value="<?php echo $productId; ?>">
            <div class="ProductSpecs"> Quantity: <input style="width:25px;text-align:center" type="number" name="quantity" value="1"> </div>
            <input class="cartAddBtn" style="background-color:transparent;height:40px;" type="submit" value="Add to Cart" >
            </form>
            <br><br>
        </div>
        <div class="bx">
            <div class="changePhoto" onclick="PreviousPhoto()"></div>
            <div class="circle circleSelected" id="circle1"></div>
            <div class="circle" id="circle2"></div>
            <div class="circle" id="circle3"></div>
            <div class="circle" id="circle4"></div>
            <div class="circle" id="circle5"></div>
            <div class="changePhoto" style="margin-left:5px;border-left: 10px solid white;border-right: 10px solid transparent" onclick="NextPhoto()"></div>
        </div>
        <p id='fadeObject' class="visible" style='color:green;float:right;margin-right:21px;'>
        <?php
          if(isset($_GET['msg'])){
            $msg = $_GET['msg'];
            if ($msg == "succ"){
              $CartMsg = "Product(s) added succesfully";
              echo $CartMsg;
            }
          }
        ?>
        </p>

     </div>

     <div class="adtop" style="margin-top:20px;"> <img class="ad2" src="img/ad2.jpg" alt=""> </div>
     
  </body>
</html>
