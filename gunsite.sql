-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2019 at 10:55 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gunsite`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `pass`) VALUES
(1, 'admintest', 'admin1');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id_cart` int(2) NOT NULL,
  `user_fk` int(3) NOT NULL,
  `product_fk` int(3) NOT NULL,
  `quantity` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id_ca` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id_ca`, `Name`) VALUES
(1, 'News'),
(2, 'User Posts'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id_c` int(2) NOT NULL,
  `author_fk` int(2) NOT NULL,
  `text` text NOT NULL,
  `post_fk` int(2) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id_c`, `author_fk`, `text`, `post_fk`, `date`) VALUES
(1, 1, 'ra magariaaaaa', 1, '2019-05-20'),
(2, 2, 'Very good news', 1, '2019-04-12'),
(3, 3, 'getanxmebi brat', 2, '2019-05-06'),
(18, 2, 'aba raaa', 3, '2019-05-05'),
(19, 1, 'magari topia, me maq da yovel dge vsroulob', 3, '2019-05-05'),
(20, 3, 'genialuria', 5, '2019-05-06'),
(21, 1, 'gmadlobt, gmadlobt', 5, '2019-05-06');

-- --------------------------------------------------------

--
-- Table structure for table `ordersproducts`
--

CREATE TABLE `ordersproducts` (
  `id` int(11) NOT NULL,
  `product_fk` int(2) NOT NULL,
  `order_fk` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ordersproducts`
--

INSERT INTO `ordersproducts` (`id`, `product_fk`, `order_fk`) VALUES
(3, 1, 3),
(4, 2, 3),
(6, 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `orderstable`
--

CREATE TABLE `orderstable` (
  `id_ord` int(2) NOT NULL,
  `user_fk` int(2) NOT NULL,
  `price` int(5) NOT NULL,
  `keyy` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orderstable`
--

INSERT INTO `orderstable` (`id_ord`, `user_fk`, `price`, `keyy`) VALUES
(3, 1, 7100, 5647),
(7, 1, 1300, 89325);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id_p` int(2) NOT NULL,
  `author_fk` int(2) NOT NULL,
  `date` date NOT NULL,
  `text` text NOT NULL,
  `heading` varchar(50) NOT NULL,
  `img` varchar(100) NOT NULL,
  `category_fk` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id_p`, `author_fk`, `date`, `text`, `heading`, `img`, `category_fk`) VALUES
(1, 1, '2019-04-11', 'The United States Army\'s Delta Force, at the request of R&D NCO Larry Vickers, collaborated with the German arms maker Heckler & Koch to develop the new carbine in the early 1990s.[when?] During development, Heckler & Koch capitalized on experience gained developing the Bundeswehr\'s Heckler & Koch G36 assault rifle, the U.S. Army\'s XM8 rifle project (cancelled in 2005) and the modernization of the British Armed Forces SA80 small arms family.[citation needed] The project was originally called the Heckler & Koch M4, but this was changed in response to a trademark infringement suit filed by Colt Defense.\r\n\r\nDelta Force replaced its M4s with the HK416 in 2004, after tests revealed that the piston operating system significantly reduces malfunctions while increasing the life of parts.[4][dead link] The HK416 has been tested by the United States military and is in use with some law enforcement agencies. It has been adopted as the standard rifle of the Norwegian Armed Forces (2008) and the French Armed Forces (2017) and is used by many special operations units worldwide.\r\n\r\nA modified variant underwent testing by the United States Marine Corps as the M27 Infantry Automatic Rifle. After the Marine Corps Operational Test & Evaluation Activity supervised a round of testing at MCAGCC Twentynine Palms, Fort McCoy, and Camp Shelby (for dust, cold-weather, and hot-weather conditions, respectively). As of March 2012, fielding of 452 IARs has been completed of 4,748 ordered. Five infantry battalions: 1st Light Armored Reconnaissance Battalion and 2nd Battalion, 4th Marines, out of Camp Pendleton, Calif.; First Battalion, 3rd Marines, out of Marine Corps Base Hawaii; 1st Battalion, 9th Marines, out of Camp Lejeune, N.C.; and 1st Battalion, 25th Marines, out of Fort Devens, Mass. have deployed the weapon.[5][6] In December 2017, the Marine Corps revealed a decision to equip every Marine in an infantry squad with the M27.[7]', 'Hk introduces 416', '416.jpg', 1),
(2, 1, '2019-04-18', 'The AK-74 is an assault rifle developed in the early 1970s by Russian designer Mikhail Kalashnikov as the replacement for the earlier AKM (itself a refined version of the AK-47). It uses a smaller 5.45×39mm cartridge, replacing the 7.62×39mm chambering of earlier Kalashnikov-pattern weapons.\r\n\r\nThe rifle first saw service with Soviet forces engaged in the 1979 Afghanistan conflict.[8] The head of the Afghan bureau of the Pakistani Inter-Services Intelligence claimed that the CIA paid $5,000 for the first AK-74 captured by the Mujahideen during the Soviet–Afghan War.[9]\r\n\r\nCurrently, the rifle continues to be used by the majority of countries of the former Soviet Union. Additionally, licensed copies were produced in Bulgaria (AK-74, AKS-74 and AKS-74U), and the former East Germany (MPi-AK-74N, MPi-AKS-74N, MPi-AKS-74NK).[8][10][11]\r\nThe rifle first saw service with Soviet forces engaged in the 1979 Afghanistan conflict.[8] The head of the Afghan bureau of the Pakistani Inter-Services Intelligence claimed that the CIA paid $5,000 for the first AK-74 captured by the Mujahideen during the Soviet–Afghan War.[9]', 'The legacy of AK-74', 'ak.jpg', 2),
(3, 2, '2019-04-05', 'The MP5 (German: Maschinenpistole 5) is a 9x19mm Parabellum submachine gun, developed in the 1960s by a team of engineers from the German small arms manufacturer Heckler & Koch GmbH (H&K) of Oberndorf am Neckar. There are over 100 variants of the MP5,[5] including some semi-automatic versions.\r\n\r\nThe MP5 is one of the most widely used submachine guns in the world,[6] having been adopted by 40 nations and numerous military, law enforcement, intelligence, and security organizations.[4] It was widely used by SWAT teams in North America, but has largely been supplanted by AR-15 variants in the 21st century.\r\n\r\nIn 1999, Heckler & Koch developed the Heckler & Koch UMP, the MP5\'s successor;[7] both are available as of 2019.', 'Short History of MP 5', 'mp5.jpg', 2),
(5, 1, '2019-05-06', '   The non-reciprocating charging handle on the top of the receiver cover is used to retract the bolt. Variants have a ratchet safety mechanism which will catch the bolt and lock its movement if it is retracted past the magazine, but not far enough to engage the sear. When the handle is fully retracted to the rear, the bolt will cock (catch) on the sear mechanism and the handle and cover are released to spring fully forward under power of a small spring. The cover will remain forward during firing since it does not reciprocate with the bolt. The military and police versions will fire immediately upon chambering a cartridge as the Uzi is an open bolt weapon.\r\n\r\nThere are two external safety mechanisms on the Uzi. The first is the three-position selector lever located at the top of the grip and behind the trigger group. The rear position is \"S\", or \"safe\" (S = Sicher or Secure on the MP2), which locks the sear and prevents movement of the bolt.\r\n\r\nThe second external safety mechanism is the grip safety, which is located at the rear of the grip. It is meant to help prevent accidental discharge if the weapon is dropped or the user loses a firm grip on the weapon during firing.\r\n\r\nThe trigger mechanism is a conventional firearm trigger, but functions only to control the release mechanism for either the bolt (submachine gun) or firing pin holding mechanism (semi-auto) since the Uzi does not incorporate an internal cocking or hammer mechanism. While the open-bolt system is mechanically simpler than a closed-bolt design (e.g. Heckler & Koch MP5), it creates a noticeable delay between when the trigger is pulled and when the gun fires.\r\n\r\nThe magazine release button or lever is located on the lower portion of the pistol grip and is intended to be manipulated by the non-firing hand. The paddle-like button lies flush with the pistol grip in order to help prevent accidental release of the magazine during rigorous or careless handling.\r\n\r\nWhen the gun is de-cocked the ejector port closes, preventing entry of dust and dirt. Though the Uzi\'s stamped-metal receiver is equipped with pressed reinforcement slots to accept accumulated dirt and sand, the weapon can still jam with heavy accumulations of sand in desert combat conditions when not cleaned regularly.[18] The magazine must be removed prior to de-cocking the weapon.          ', 'Operation circle of Uzi', 'Uzi_of_the_israeli_armed_forces.jpg', 3),
(7, 4, '2019-05-12', 'The Å korpion was developed in the late 1950s by Miroslav RybÃ¡Å™ with the working name \"model 59\". The design was completed in 1961 and named \"Samopal Vz. 61\".[2] It was subsequently adopted by the Czechoslovak Army and security forces, and later exported to various countries. Yugoslavia produced a version under licence. It was also used by armed groups,[4] including the Irish Republican Army, Irish National Liberation Army and the Italian Red Brigades. The latter used the Å korpion during the 1978 kidnapping of Aldo Moro, also using this weapon to kill Moro.[2][5] In the 1990s the Gang de Roubaix used the Å korpion in a series of attacks in France.[2] In 2017 police in Sweden estimated that about 50 formerly deactivated weapons from Slovakia were in circulation among criminals in Sweden.[6]', 'The greatest PDW: vz61', 'Submachine_gun_vz61.jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id_pr` int(2) NOT NULL,
  `price` int(5) NOT NULL,
  `name` varchar(20) NOT NULL,
  `mainImg` varchar(100) NOT NULL DEFAULT 'defaultProduct.jpg',
  `img1` varchar(100) NOT NULL DEFAULT 'defaultProduct.jpg',
  `img2` varchar(100) NOT NULL DEFAULT 'defaultProduct.jpg',
  `img3` varchar(100) NOT NULL DEFAULT 'defaultProduct.jpg',
  `img4` varchar(100) NOT NULL DEFAULT 'defaultProduct.jpg',
  `category` varchar(20) NOT NULL,
  `availability` varchar(20) NOT NULL DEFAULT 'In stock',
  `caliber` varchar(10) NOT NULL,
  `weight` int(2) NOT NULL,
  `conditionn` varchar(20) NOT NULL,
  `manufacturer` varchar(20) NOT NULL,
  `isProduct` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id_pr`, `price`, `name`, `mainImg`, `img1`, `img2`, `img3`, `img4`, `category`, `availability`, `caliber`, `weight`, `conditionn`, `manufacturer`, `isProduct`) VALUES
(1, 1300, 'Ak-74', 'ak1.jpg', 'ak2.jpg', 'ak3.jpg', 'ak4.jpg', 'ak0.jpg', 'Hunting Weapons', 'In stock', '5.45X39', 3, 'New', 'Kalashnikov Concern', 1),
(2, 900, 'Tec-9', 'tec-9.jpg', 'defaultProduct.jpg', 'defaultProduct.jpg', 'defaultProduct.jpg', 'defaultProduct.jpg', 'Personal Defence', 'In stock', '9X19mm', 1, 'New', 'Intratec', 1),
(6, 4000, 'SCAR', 'FN-Scar-16s-FDE.jpg', 'defaultProduct.jpg', 'defaultProduct.jpg', 'defaultProduct.jpg', 'defaultProduct.jpg', 'Hunting Weapons', 'In stock', '5.45X45 mm', 4, 'New', 'FN', 1),
(7, 7000, 'AUG A3 M1', '2-styaugm1mudo_1.jpg', '7-gaugm1blko_1.jpg', 'aug_m1_mud_scope_fl_r_large.jpg', 'defaultProduct.jpg', 'defaultProduct.jpg', 'Hunting Weapons', 'In stock', '5.45X45 mm', 3, 'New', 'Streyr', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_u` int(11) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Passsword` varchar(100) NOT NULL,
  `Img` varchar(50) NOT NULL DEFAULT 'defaultUser.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_u`, `Email`, `Passsword`, `Img`) VALUES
(1, 'NikaJamburia@gmail.com', 'nika12', 'received_10213341257226596.jpeg'),
(2, 'vigaca@gmail.com', '12345', 'defaultUser.jpg'),
(3, 'ragaca@gmail.com', '123456', 'download.jpg'),
(4, 'AdminTest', '$2y$10$gUsQQF1jwA2nETvkqQ1B8uVkPncJScWIHSz6zKEJXs4vtDFg7mwE2', 'adminimg.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id_cart`),
  ADD KEY `user_fk` (`user_fk`),
  ADD KEY `product_fk` (`product_fk`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id_ca`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id_c`),
  ADD KEY `author_fk_com` (`author_fk`),
  ADD KEY `post_fk_com` (`post_fk`);

--
-- Indexes for table `ordersproducts`
--
ALTER TABLE `ordersproducts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_fk` (`product_fk`),
  ADD KEY `order_fk` (`order_fk`);

--
-- Indexes for table `orderstable`
--
ALTER TABLE `orderstable`
  ADD PRIMARY KEY (`id_ord`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id_p`),
  ADD KEY `author_fk` (`author_fk`),
  ADD KEY `category_fk` (`category_fk`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id_pr`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_u`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id_cart` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id_ca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id_c` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `ordersproducts`
--
ALTER TABLE `ordersproducts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orderstable`
--
ALTER TABLE `orderstable`
  MODIFY `id_ord` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id_p` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id_pr` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_u` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`user_fk`) REFERENCES `users` (`id_u`),
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`product_fk`) REFERENCES `products` (`id_pr`);

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`author_fk`) REFERENCES `users` (`id_u`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`post_fk`) REFERENCES `post` (`id_p`);

--
-- Constraints for table `ordersproducts`
--
ALTER TABLE `ordersproducts`
  ADD CONSTRAINT `ordersproducts_ibfk_1` FOREIGN KEY (`product_fk`) REFERENCES `products` (`id_pr`),
  ADD CONSTRAINT `ordersproducts_ibfk_2` FOREIGN KEY (`order_fk`) REFERENCES `orderstable` (`id_ord`);

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`author_fk`) REFERENCES `users` (`id_u`),
  ADD CONSTRAINT `post_ibfk_2` FOREIGN KEY (`category_fk`) REFERENCES `categories` (`id_ca`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
